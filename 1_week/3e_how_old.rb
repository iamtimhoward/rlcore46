=begin
Exercise 3
Write a Ruby program that displays how old I am, if I am 979000000 seconds old.
Display the result as a floating point (decimal) number to two decimal places
(for example, 17.23). Note: To format the output to say 2 decimal places, we
can use the Kernel's format method. For example, if x = 45.5678 then
format("%.2f", x) will return the string 45.57

=end

# doctest: The units of time expressed as seconds
# doctest: second
# >> second
# => 1
# doctest: minute
# >> minute
# => 60
# doctest: day
# >> day
# => 86400
# doctest: year
# >> year
# => 31536000
# doctest: age in years should be something
# >> (age_in_years * 100).to_i / 100.0
# => 31.04

age = 979000000
second = 1
minute = second * 60.0
hour = minute * 60
day = hour * 24
year = day * 365 
age_in_years = age / year

# doctest: A string in the required format
# >> format("%.2f", age_in_years)
# => "31.04"

if __FILE__ == $0
  puts "If you are " + age.to_s + " then you are " + format("%.2f", age_in_years) + " years old."
end

